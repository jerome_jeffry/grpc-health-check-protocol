module github.com/Jerome-0609/microservice

go 1.14

require (
	
	github.com/golang/protobuf v1.4.2
	github.com/lib/pq v1.5.2
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.23.0
)
