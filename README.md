#GRpc-Health-Check

Health-Check is registered to every Service.
Implemented Check Rpc and Watch Rpc in every service to get status of respective services.

To check the status of the service:
Run the services and Under grpc-health-probe run
->go run main.go -addr=localhost:4040
->go run main.go -addr=localhost:4041 
	to check the health status of services running in respective addresses.

Service initial starting time is mocked, So it takes time to start. So at first, "status: NOT-SERVING" will be shown, after 100 seconds
you can recheck to make sure "status: SERVING"

you can also run 
->go run main.go -addr=localhost:4040 -connect-timeout 250ms -rpc-timeout 100ms
	to specify connection and rpc timeouts
					   
This grpc health probe will be made to run as a container and in the K8s Pod specification manifest,
have to specify a livenessProbe and readinessProbe for this container.

K8s probe this container at specific time to know the status of services.


# go-microservices-updated

Implemented WithGrpcServer(), which will be reused by all microservices.

This simple code has 2 simple microservices(user service and device service) and a client that will communicate via grpc.  

Code workflow:

1)client calls Device microservice with device id to retrieve device and its user information.
2)Device microservice inturn calls User microservice with userid to retrieve username.
3)Device microservice sends back deviceid, uerid, username to the client.

Run from cmd/go-microservices/main.go with command line args.

steps:

1)go run main.go user ->To run user microservice
2)go run main.go device->To run device microservice
3)client/main.go -> To run a client




