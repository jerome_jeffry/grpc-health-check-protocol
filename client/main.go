package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	// "io"
	// "log"

	"github.com/Jerome-0609/microservice/proto/device"
	"github.com/Jerome-0609/microservice/proto/healthcheck/google.golang.org/grpc/health/grpc_health_v1"

	"google.golang.org/grpc"
)

//Health checking User service with Check Rpc call
func healthcheckServerCheck(ctx context.Context, conn *grpc.ClientConn) {
	hcClient := grpc_health_v1.NewHealthClient(conn)
	req := &grpc_health_v1.HealthCheckRequest{
		// Service: "device.Device",
		Service: "user.User",
	}
	if response, err := hcClient.Check(ctx, req); err == nil {
		fmt.Println("Status:", response.GetStatus())

	} else {

		if err.Error() == "deadline is exceeded" {
			//serverStatus = "Unhealthy"
			fmt.Println("server is unhealthy")
		}
		fmt.Println(err.Error())
	}
}

//Health checking Device service with Watch Rpc call (continous streaming of health check until health check passes)
func healthcheckServerWatch(ctx context.Context, conn *grpc.ClientConn) {
	hcClient := grpc_health_v1.NewHealthClient(conn)
	req := &grpc_health_v1.HealthCheckRequest{
		Service: "device.Device",
	}
	stream, err := hcClient.Watch(ctx, req)
	if err != nil {
		log.Fatal("cannot get status: ", err)
	}
	for {
		res, err := stream.Recv()
		if err == io.EOF {
			return
		}
		if err != nil {
			log.Fatal("cannot receive response: ", err)
		}

		status := res.GetStatus()
		log.Print("status: ", status)
	}
}

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 120*time.Second)
	defer cancel()

	//Dialing Userservice address
	conn, err := grpc.Dial("localhost:4041", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	//health-check User service by Check method
	healthcheckServerCheck(ctx, conn)

	//Dialing DeviceService address
	conn1, err := grpc.Dial("localhost:4040", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer conn1.Close()

	//health-check Device service by watch method
	//healthcheckServerWatch(ctx, conn1)

	//Device service client
	client := device.NewDeviceClient(conn1)

	//hardcoded for time being
	deviceid := "abcde"

	req1 := &device.Request{
		DeviceId: deviceid,
	}
	if response1, err := client.FetchData(ctx, req1); err == nil {
		fmt.Println("UserId:", response1.GetUserId())
		fmt.Println("UserName:", response1.GetUserName())
		fmt.Println("DeviceID", response1.GetDeviceId())
	} else {
		fmt.Println(err.Error())
	}

}
