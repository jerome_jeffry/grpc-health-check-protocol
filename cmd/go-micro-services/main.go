package main

import (
	"fmt"
	"log"
	"net"
	"os"

	Deviceservice "github.com/Jerome-0609/microservice/device"
	Userservice "github.com/Jerome-0609/microservice/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type Register interface {
	RegisterGrpcService(srv *grpc.Server)
	GetServerAddr() string
	RegisterHealthService(srv *grpc.Server)
}

type Option func(i interface{})

//WithGrpcServer() will be resused by all microservices
func WithGrpcserver() Option {
	return func(i interface{}) {
		if v, ok := i.(Register); ok {
			serverAddr := v.GetServerAddr()
			listener, err := net.Listen("tcp", serverAddr)
			if err != nil {
				panic(err)
			}
			srv := grpc.NewServer()
			v.RegisterGrpcService(srv)
			v.RegisterHealthService(srv)
			fmt.Print(serverAddr, "\n")
			reflection.Register(srv)
			if e := srv.Serve(listener); e != nil {
				panic(e)
			}
		}

	}
}

//user service runs on 4041
//device service runs on 4040
func main() {

	var (
		useraddr   string = "localhost:4041"
		deviceaddr string = "localhost:4040"
	)
	// var srv server
	switch os.Args[1] {
	case "user":
		//mocking User service up time
		go Userservice.Startdb()
		Userservice.NewUser(useraddr, WithGrpcserver())
	case "device":
		//mocking Device service up time
		go Deviceservice.Startdb()
		Deviceservice.NewDevice(deviceaddr, getclient(useraddr), WithGrpcserver())
	default:
		log.Fatalf("unknown command %s", os.Args[1])

	}

}

func getclient(addr string) *grpc.ClientConn {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	return conn
}
