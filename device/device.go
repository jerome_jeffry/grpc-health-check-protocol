package Deviceservice

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/Jerome-0609/microservice/proto/device"
	"github.com/Jerome-0609/microservice/proto/healthcheck/google.golang.org/grpc/health/grpc_health_v1"
	"github.com/Jerome-0609/microservice/proto/user"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var isDbstarted = false

type Device struct {
	serverAddr string
	UserClient user.UserClient
}

//Device constructor
func NewDevice(addr string, con *grpc.ClientConn, grpcServer func(interface{})) *Device {
	// const (
	// 	defserverAddr = "localhost:4040"
	// )

	d := Device{addr, user.NewUserClient(con)}
	grpcServer(&d)
	return &d
}

//Registering Device service to GRpc server
func (dev *Device) RegisterGrpcService(srv *grpc.Server) {
	device.RegisterDeviceServer(srv, dev)
	fmt.Print("device service started on: ")

}

//Registering Health-Check service to GRpc server
func (dev *Device) RegisterHealthService(srv *grpc.Server) {
	grpc_health_v1.RegisterHealthServer(srv, dev)

}

//Returning Device service address
func (dev *Device) GetServerAddr() string {
	return dev.serverAddr
}

//mock DB
func Startdb() {
	sleepTime := 50
	log.Println("Connecting to db")
	time.Sleep(time.Duration(sleepTime) * time.Second)
	log.Println("Database is ready!")
	isDbstarted = true

}

//device microservice sends deviceid, userid, username to client on calling FetchData()
func (dev *Device) FetchData(ctx context.Context, req *device.Request) (res *device.Result, err error) {
	if isDbstarted == true {
		devID := req.GetDeviceId()
		if err := contextError(ctx); err != nil {
			return nil, err
		}

		//Device service have deviceid and userid (db/Inmemory)
		//for time being hardcoded

		userid := "123"
		inp := &user.Request{
			UserId: userid,
		}
		username := ""
		if response, err := dev.UserClient.FetchData(context.Background(), inp); err == nil {
			fmt.Println("hit user service")
			username = response.GetUserName()
			fmt.Println("Received username:", username)
		} else {
			return nil, logError(status.Errorf(codes.Internal, "cannot fetch data: %v", err))
		}

		//returning the response
		res = &device.Result{
			DeviceId: devID,
			UserId:   userid,
			UserName: username,
		}
	}
	return res, nil

}

//Check Rpc returns the health status of Device service (Unary GRpc)
func (dev *Device) Check(ctx context.Context, req *grpc_health_v1.HealthCheckRequest) (res *grpc_health_v1.HealthCheckResponse, err error) {
	if err := contextError(ctx); err != nil {
		return nil, err
	}

	log.Println("health checking in Device service")
	//service status will be NOT_SERVING until DB is up and running.
	if isDbstarted == true {
		log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_SERVING)
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_SERVING,
		}, nil
	} else if isDbstarted == false {
		log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_NOT_SERVING)
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_NOT_SERVING,
		}, nil
	} else {
		log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_UNKNOWN)
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_UNKNOWN,
		}, nil
	}

}

//Watch Rpc returns the streaming health status of Device service.
func (dev *Device) Watch(req *grpc_health_v1.HealthCheckRequest, stream grpc_health_v1.Health_WatchServer) error {

	var res *grpc_health_v1.HealthCheckResponse
	// service := req.GetService()
	// log.Printf("Health checking for: ", service)
	ctx := stream.Context()
	if err := contextError(ctx); err != nil {
		return err
	}
	log.Println("health checking in device service")
	for {
		if isDbstarted == true {
			log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_SERVING)
			res = &grpc_health_v1.HealthCheckResponse{
				Status: grpc_health_v1.HealthCheckResponse_SERVING,
			}
			err := stream.Send(res)
			if err != nil {
				return err
			}
			break
		} else if isDbstarted == false {
			log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_NOT_SERVING)
			res = &grpc_health_v1.HealthCheckResponse{
				Status: grpc_health_v1.HealthCheckResponse_NOT_SERVING,
			}
			err := stream.Send(res)
			if err != nil {
				return err
			}
		} else {
			log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_UNKNOWN)
			res = &grpc_health_v1.HealthCheckResponse{
				Status: grpc_health_v1.HealthCheckResponse_UNKNOWN,
			}
			err := stream.Send(res)
			if err != nil {
				return err
			}
		}

	}
	return nil

}

//checking context errors
func contextError(ctx context.Context) error {
	switch ctx.Err() {
	case context.Canceled:
		return logError(status.Error(codes.Canceled, "request is canceled"))
	case context.DeadlineExceeded:
		return logError(status.Error(codes.DeadlineExceeded, "deadline is exceeded"))
	default:
		return nil
	}
}

func logError(err error) error {
	if err != nil {
		log.Print(err)
	}
	return err
}

//starting Grpc server on 4040
// func (dev *Device) Run() {
// 	// listener, err := net.Listen("tcp", ":4040")
// 	// if err != nil {
// 	// 	panic(err)
// 	// }
// 	fmt.Println("device service started")

// 	// srv := grpc.NewServer()
// 	// device.RegisterDeviceServer(srv, dev)
// 	// reflection.Register(srv)

// 	// if e := srv.Serve(listener); e != nil {
// 	// 	panic(e)
// 	// }

// }
